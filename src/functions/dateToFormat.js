export default function dateToFormat(date) {
	let year = date.replace( dateRegExp, '$1')
	let month = date.replace( dateRegExp, '$2')
	let day = date.replace( dateRegExp, '$3')
	let divider = date.replace( dateRegExp, '$4')
	let time = date.replace( dateRegExp, '$5$6')


	month =	( String(month).length < 2 ) ? '0'+month : month
	day =	( String(day).length < 2 ) ? '0'+day : day

	let formattedDate = year + '-' + month + '-' + day + divider + time

	if(debug) console.log( 'dateToFormat before:', date, 'after:', formattedDate )
	return formattedDate
}