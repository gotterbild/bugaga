import dateMonthFromNumber from './dateMonthFromNumber';

export default function dateHumanize(date) {
	let year = date.replace( dateRegExp, '$1')
	let month = date.replace( dateRegExp, '$2')
	let day = date.replace( dateRegExp, '$3')

	// if(debug) console.log( 'Humanizing', date, 'year:', year, 'month:', month, 'day:', day )

	month = dateMonthFromNumber(month)

	return day + ' ' + month + ' ' + year;
}