export default function isRecordInViewport(id) {
	let el = document.getElementById( 'record'+id )
  let rect = el.getBoundingClientRect()

	if(debug) console.log(  )

	let isInViewPort = (
    rect.top >= 0 &&
    rect.bottom <= (window.innerHeight || document.documentElement.clientHeight)
  )
	if(debug) console.log(
		'isRecordInViewport:', rect,
		'\nwindow.innerHeight:', window.innerHeight,
		'\ndocument.documentElement.clientHeight:', document.documentElement.clientHeight,
		'\nrect.top >= 0', rect.top >= 0,
		'\nrect.bottom <= (window.innerHeight || document.documentElement.clientHeight)', rect.bottom <= (window.innerHeight || document.documentElement.clientHeight),
		'\nisInViewPort:', isInViewPort
	)

  return isInViewPort
}