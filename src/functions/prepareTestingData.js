import generateHash from './generateHash'

export default function prepareTestingData() {

	if(debug) console.log( 'prepareTestingData fired' )

	let icons = [
    [
      'food','fast-food','coffee','cart','paw',
      'clean','wrench','working','home','sofa',
      'shirt','dress','child','signal-2','wifi',
    ],
    [
      'bicycle','motorcycle','cab','bus','subway',
      'flight','suitcase','globe-1','sun-filled','glass',
      'mobile','laptop','android','apple','gift',
    ],
    [
      'music','gamepad','book','brush','flowerpot',
      'weight','skate','soccer-ball','tree','mountain',
      'cut','lips','leaf','heart','heartbeat',
    ],
    [
      'wallet','credit-card-alt','paypal','webmoney','yandex-rect',
      'rouble','dollar','euro','bitcoin','money',
      'calendar','chart-line','bank','mail-alt','handshake',
    ],
    [
      'female','male','graduation-cap','idea','ballon',
      'thumbs-up-alt','thumbs-down-alt','attention-alt','help',
      'lock','star','trash',
    ],
  ]
	let excludedIcons = [ 'cog','cw','list','users','clock','floppy','down-small','left-small','right-small','up-small','info','spin6','plus','cancel','ok','chart-pie','pencil', ]
  let toReplace = [ 'dress (file isready, just replace)', 'mobile', 'laptop', 'handshake (more square icon needed)']
  let toDelete = [ 'chair', ]

  let colors = {

    pale: {
      grey: "#cad4d8",
      red: "#e9abaa",
      orange: "#f2cd97",
      yellow: "#f2e6ab",
      green: "#c6ddb1",
      sea: "#97c4c0",
      teal: "#a1dfe8",
      blue: "#98b9dd",
      purple: "#cfa6d9",
      pink: "#edbbcd",
    },

    bright: {
      grey: '#607D8B',
      red: '#e53935',
      orange: '#FF9800',
      yellow: '#FDD835',
      green: '#8BC34A',
      sea: '#00796B',
      teal: '#26C6DA',
      blue: '#1565C0',
      purple: '#9C27B0',
      pink: '#F06292',
    },

    neutral: {
      greyDark: '#3b4c54',
    },

  }

  let user = {
    email: '',
    password: '********',
    themeColor: 'yellow',
    themeColorCode: 'hsl(  50, 73, 81 )',
  }

  let recordsQty = 40
	let priceRange = 30000

  let groups = {
    0: {
      id: 0,
      icon: 'A',
      categories: {
        0: 0,
        1: 1,
        2: 2,
        3: 3,
      },
    },
    1: {
      id: 1,
      icon: 'B',
      categories: {
        0: 4,
        1: 5,
        2: 6,
        3: 7,
        4: 8,
        5: 9,
        6: 10,
        7: 11,
        8: 12,
        9: 19,
        10: 20,
        11: 21,
      },
    },
    2: {
      id: 2,
      icon: 'C',
      categories: {
        0: 13,
        1: 14,
        2: 15,
        3: 16,
        4: 17,
        5: 18,
      },
    },
    999: {
      id: 999,
      icon: 'clock',
      categories: {},
    },
  }

  let categories = {
    0: {
      id: 0,
      name: 'Без категории',
      icon: 'icon-help',
      color: 'grey',
    },
    1: {
      id: 1,
      name: 'Еда/Хоз',
      icon: 'icon-food',
      color: 'orange',
    },
    2: {
      id: 2,
      name: 'Отдых',
      icon: 'icon-coffee',
      color: 'green',
    },
    3: {
      id: 3,
      name: 'Транспорт',
      icon: 'icon-bus',
      color: 'red',
    },
    4: {
      id: 4,
      name: 'Одежда',
      icon: 'icon-shirt',
      color: 'grey',
    },
    5: {
      id: 5,
      name: 'Инвестиции',
      icon: 'icon-chart-line',
      color: 'purple',
    },
    6: {
      id: 6,
      name: 'Комфорт',
      icon: 'icon-chair',
      color: 'blue',
    },
    7: {
      id: 7,
      name: 'Техника',
      icon: 'icon-mobile',
      color: 'sea',
    },
    8: {
      id: 8,
      name: 'Здоровье',
      icon: 'icon-heartbeat',
      color: 'pink',
    },
    9: {
      id: 9,
      name: 'Подарки',
      icon: 'icon-gift',
      color: 'teal',
    },
    10: {
      id: 10,
      name: 'Путешествия',
      icon: 'icon-suitcase',
      color: 'yellow',
    },
    11: {
      id: 11,
      name: 'Квартира',
      icon: 'icon-home',
      color: 'red',
    },
    12: {
      id: 12,
      name: 'Связь',
      icon: 'icon-signal-2',
      color: 'green',
    },
    13: {
      id: 13,
      name: 'Paypal',
      icon: 'icon-paypal',
      color: 'blue',
    },
    14: {
      id: 14,
      name: 'Яндекс',
      icon: 'icon-yandex-rect',
      color: 'yellow',
    },
    15: {
      id: 15,
      name: 'Кредит',
      icon: 'icon-bank',
      color: 'sea',
    },
    16: {
      id: 16,
      name: 'Погрешность',
      icon: 'icon-cancel',
      color: 'red',
    },
    17: {
      id: 17,
      name: 'Паша',
      icon: 'icon-male',
      color: 'blue',
    },
    18: {
      id: 18,
      name: 'Катя',
      icon: 'icon-female',
      color: 'green',
    },
    19: {
      id: 19,
      name: 'Ещё одна категория с длинным названием',
      icon: 'icon-female',
      color: 'sea',
    },
    20: {
      id: 20,
      name: 'Test',
      icon: 'icon-male',
      color: 'red',
    },
    21: {
      id: 21,
      name: 'Test2',
      icon: 'icon-cancel',
      color: 'yellow',
    },

    999: {
      id: 999,
      name: 'Временное',
      icon: 'icon-clock',
      color: 'grey',
    },
    1000: {
      id: 1000,
      name: 'Системная категория',
      icon: 'icon-help',
      color: 'white',
    },
    1001: {
      id: 1001,
      name: 'Добавить запись',
      icon: 'icon-plus',
      color: 'white',
    },
  }

	let records = []
  for( let i=0; i < recordsQty; i++ ){
  	let cats = generateCategoriesForRecord()
  	let date = randomDate( new Date(2015, 7, 1), new Date() )
  	let sign = generateSign( cats[0] )
  	let record = {
      id: i,
      hash: generateHash(),
      datetime: date,
      modified: date,
      name: categories[ cats[0] ].name,
      price: generatePrice( sign ),
      sign: sign,
      categories: cats,
  	}
  	records.push(record)
  }

  if( records.length > 1 ){
    records.sort(compare)
  }


	let answer = {
    user: user,
    colors: colors,
    icons: icons,
		groups: groups,
		categories: categories,
		records: records,
	}

  return answer

  ///////////////////////////////////////////////// FUNCTIONS ////////////////////////////////////////////////////

  function compare(a,b) {
    if( a.datetime < b.datetime ){
      // if(debug) console.log( a.datetime, '<', b.datetime )
      return 1
    }else if( a.datetime > b.datetime )
      // if(debug) console.log( a.datetime, '>', b.datetime )
      return -1
    return 0
  }

	function generateCategoriesForRecord() {
		let count = getRandomInt( 1, 3 )

		let random = Math.random()
		if( random < 0.8 ){
			count = 1
		}else if( 0.8 <= random <= 0.98 ){
			count = 2
		}else{
			count = 3
		}

		let categories = {}
		for( let i=0; i < count; i++ ){
  		let id = getRandomInt( 1, 21 )
			categories[i] = id
		}
		return categories
	}

	function generateSign(id) {
		switch( Number( categories[id].id ) ){

			case 13:
			case 17:
				return 'plus'
			break

			case 9:
			case 16:
			case 18:
			case 20:
			case 21:
				if( Math.random > 0.5 ){
					return 'plus'
				}else{
					return 'minus'
				}
			break

			default:
				return 'minus'
		}
	}

	function generatePrice(sign) {
    let price
    if( sign == 'minus' ){
  		price = getRandomInt( 0, priceRange/20 )
    }else{
      price = getRandomInt( 0, priceRange )
    }
		// price = price.toFixed(2)
    price = Number(price)
    price = ( price < 45 ) ? 45 : price
		return price
	}

	function randomDate( start, end ) {
		let randomDate = new Date( start.getTime() + Math.random() * (end.getTime() - start.getTime()) )
		randomDate = randomDate.toISOString()
		return randomDate
	}

	function getRandomInt( min, max ) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
	}

	function getRandomFloat( min, max ) {
	  return Math.random() * (max - min) + min;
	}

}