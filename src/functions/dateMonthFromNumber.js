export default function dateMonthFromNumber(number) {
	let month = ''

	switch(number) {

		case 1:
		case '1':
		case '01': month = 'янв'; break;

		case 2:
		case '2':
		case '02': month = 'фев'; break;

		case 3:
		case '3':
		case '03': month = 'мар'; break;

		case 4:
		case '4':
		case '04': month = 'апр'; break;

		case 5:
		case '5':
		case '05': month = 'мая'; break;

		case 6:
		case '6':
		case '06': month = 'июн'; break;

		case 7:
		case '7':
		case '07': month = 'июл'; break;

		case 8:
		case '8':
		case '08': month = 'авг'; break;

		case 9:
		case '9':
		case '09': month = 'сен'; break;

		case 10:
		case '10': month = 'окт'; break;

		case 11:
		case '11': month = 'ноя'; break;

		case 12:
		case '12': month = 'дек'; break;
	}

	return month
}