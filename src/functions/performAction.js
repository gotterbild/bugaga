import dateToFormat from './dateToFormat'

export default function performAction(action) {
  let change = {}
  let now = new Date()
  let datetime = ''
  let buttons = {}

  if(debug) console.log( 'performing:', action )

  switch(action) {

    /////////////////////////////////////////////// List Page //////////////////////////////////////////////////

    case 'recordAdd':
      store.commit( 'addRecordProcedureStart' )
      store.commit( 'scrollToCurrentRecord' )
      store.commit( 'changeAppState', 'recordAdd' )
    break

    case 'recordEdit':
      store.commit( 'changeAppState', action )
    break

    case 'unselectRecords':
      store.commit( 'unselectRecords' )
      store.commit( 'changeAppState', 'listPage' )
    break

    case 'deleteRecords':
      store.commit( 'deleteRecords' )
      store.commit( 'changeAppState', 'listPage' )
    break

    //////////////////////////////////////////// Record Add/Edit ///////////////////////////////////////////////

    case 'saveRecord':
      performAction('closeRecordPanel')
    break

    case 'saveToday':
      performAction('closeRecordPanel')
      store.commit( 'updateLastId', { records: store.state.lastId.records + 1 } )
    break

    case 'saveYesterday':
      now.setDate( now.getDate() - 1 )
      datetime = now.toISOString()
      datetime = datetime.replace( dateRegExp, '$1-$2-$3T23:59:59.999' )
      if(debug) console.log( 'saving yesterday, datetime =', datetime )
      store.commit( 'changeRecord', { datetime: datetime } )
      store.commit( 'sortRecords' )
      store.commit( 'updateCurrentRecordPosition', true )
      store.commit( 'scrollToCurrentRecord' )
      performAction('closeRecordPanel')
      store.commit( 'updateLastId', { records: store.state.lastId.records + 1 } )
    break

    case 'saveAnotherDate':
      performAction('closeRecordPanel')
      store.commit( 'updateLastId', { records: store.state.lastId.records + 1 } )
    break

    case 'selectAnotherDate':
      store.commit( 'setDateSelectState', 'selectDate' )
      store.commit( 'setRecordPanelState', 'selectDate' )
    break

    case 'deleteRecord':
      store.commit( 'scrollToCurrentRecord' )
      store.commit( 'deleteRecord' )
      performAction('closeRecordPanel')
    break

    case 'cancelRecordAdd':
      store.commit('cancelRecordAdd')
     	performAction('closeRecordPanel')
    break

    case 'cancelRecordEdit':
      store.commit('cancelRecordEdit')
      performAction('closeRecordPanel')
    break

    case 'closeRecordPanel':
      store.commit( 'changeAppState', 'listPage' )
      store.commit( 'recountBalance')
      store.commit( 'setDateSelectState', '' )
      window.scroll({
        top: pageYOffset - topPanelHeight,
        behavior: 'smooth'
      })
    break

    case 'cancelAnotherDate':
      let change = {
        date: new Date().toISOString(),
      }
      store.commit( 'changeRecord', change )
      store.commit( 'setRecordPanelState', store.state.recordPanelStateLast )
      store.commit( 'setDateSelectState', '' )
    break

    /////////////////////////////////////////////// Category Panel  //////////////////////////////////////////////////

    case 'categoryAdd':
      store.commit( 'setCategoryPanelState', 'categoryAdd' )
      store.commit( 'setCategoryPanelLocalState', 'selectIconInitial' )
    break

    case 'cancelCategoryAdd':
      store.commit( 'setCategoryPanelState', 'categoryColor' )
    break

    case 'cancelCategoryEdit':
      store.commit( 'setCategoryPanelState', 'categoryColor' )
    break

    case 'cancelSelectIcon':
      store.commit( 'setCategoryPanelLocalState', 'categoryColor' )
    break

    case 'showSelectGroup':
      store.commit( 'setCategoryPanelLocalState', 'selectGroup' )
    break

    case 'showColors':
      // store.commit( 'setCategoryPanelLocalState', 'categoryColor' )
    break

    case 'cancelSelectGroup':
      store.commit( 'setCategoryPanelLocalState', 'categoryColor' )
    break



  }

}