export default function generateHash( length = 16 ) {

	function dec2hex(dec) {
	  return ('0' + dec.toString(16)).substr(-2)
	}

  let arr = new Uint8Array( length / 2 )
  window.crypto.getRandomValues(arr)

  return Array.from(arr, dec2hex).join('')
}