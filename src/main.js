// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import {store} from './store'
// import storageAvailable from 'storage-available';
// import MemoryStorage from 'memorystorage';
// import VeeValidate from 'vee-validate';

if( process.env.NODE_ENV == "development" ){
  // parameters for development
  window.dev = true;
  window.debug = true;
}else{
  // parameters for production
  window.dev = false;
  window.debug = false;
}

window.Vue = Vue;
window.router = router;
window.store = store;
// window.Storage = '';
window.dateRegExp = /(\d+)-(\d+)-(\d+)( |T)([\d:]+)\.?(.*)/
window.transitionTime = 0.33
window.defaultTransition = transitionTime+'s all ease'

Vue.config.productionTip = false

require('smoothscroll-polyfill').polyfill();

////////////////////////////////////////////////////// FILTERS ///////////////////////////////////////////////////////////

Vue.filter( 'capitalize', function(value) {
  let splitStr = value.toLowerCase().split(' ');
  for (let i = 0; i < splitStr.length; i++) {
    // You do not need to check if i is larger than splitStr length, as your for does that for you
    // Assign it back to the array
    splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
  }
  // Directly return the joined string
  return splitStr.join(' ');
});


////////////////////////////////////////////////////// INIT ///////////////////////////////////////////////////////////

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})