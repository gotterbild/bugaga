import Vue from 'vue'
import Vuex from 'vuex'

import dateToFormat from './functions/dateToFormat'
import generateHash from './functions/generateHash'
import isRecordInViewport from './functions/isRecordInViewport'

Vue.use(Vuex);

let defaultRecordName = 'Новая запись'
let date = new Date

export let store = new Vuex.Store({
  state: {
    pageToGo: '/',
//    shopUrl: 'http://www.crowleymarine.com',
    test: 'Test successfull',
    titleAdd: ' | Bugagaлтеризация',

    user: {
        email: '',
        password: '********',
        themeColor: 'yellow',
        themeColorCode: 'hsl(  50, 73, 81 )',
    },

    balance: 0,
    balancePlus: 0,
    balanceMinus: 0,

    appState: 'listPage',
    recordPanelState: 'calculator',
    recordPanelStateLast: 'calculator',
    categoryPanelState: '',
    categoryPanelLocalState: 'categoryColor',
    categoryPanelLocalStatePrevious: 'categoryColor',
    dateSelectState: '',

    lastId: {
      records: 0,
      categories: 0,
      catGroups: 0,
    },

    todayDay: date.getUTCDate(),
    todayMonth: date.getUTCMonth() + 1, //months from 1-12
    todayYear: date.getUTCFullYear(),

    currentRecordID: 0,
    currentRecordPosition: 0,
    blankRecord: {
      blankRecord: true,
      id: 0,
      datetime: '',
      name: 'Добавить запись',
      price: '',
      sign: 'neutral',
      finalsign: 'neutral',
      negative: false,
      deleted: false,
      selected: false,
      categories: {
        0: 1001,
      },
    },
    savedCurrentRecordState: {},
    recordsSelected: {},

    currentCategoryGroup: 0,

    records: [],
    categories: {
      0: {
        id: 0,
        name: 'Без категории',
        icon: 'icon-help',
        color: 'grey',
        selected: false,
        deleted: false,
        group: 0,
      },
      1000: {
        id: 1000,
        name: 'Системная категория',
        icon: 'icon-help',
        color: 'white',
      },
      1001: {
        id: 1001,
        name: 'Добавить запись',
        icon: 'icon-plus',
        color: 'white',
      },
      999: {
        id: 999,
        name: 'Временное',
        icon: 'icon-clock',
        color: 'grey',
        selected: false,
        group: 999,
      },
    },

    groups: {
      0: {
        id: 0,
        icon: 'A',
        categories: {},
      },
      1: {
        id: 1,
        icon: 'B',
        categories: {},
      },
      2: {
        id: 2,
        icon: 'C',
        categories: {},
      },
      999: {
        id: 999,
        icon: 'clock',
        categories: {},
      },
    },

    icons: [],

    colors: {},

    requests: {},
    mustBeReloaded: [],
  },
  mutations: {

    ///////////////////////////////////////////////////// COMMON ///////////////////////////////////////////////////////

    changePageToGo( state, page ) {
      if(debug) console.log( "changePageToGo:", page )
      state.pageToGo = page
    },

    changeAppState( state, newState ) {
      if(debug) console.log( "changeAppState:", newState )
      state.appState = newState
    },

    prepareData( state, data ) {
      if(debug) console.log( "prepareData:", data )
      state.user = data.user
      state.colors = data.colors
      state.records = data.records
      state.categories = data.categories
      state.groups = data.groups
    },

    scrollToCurrentRecord( state ) {
      Vue.nextTick( function() {
        if(debug) console.log( "scrollToCurrentRecord" )
        let id = state.currentRecordID
        let record = document.getElementById( 'record'+id )
        let recOffset

// !!!!!!!!!!!!!!!!!!
        // if !topPanelHide => + topPanelOffset else topPanelOffset = 0

        // if( state.currentRecordPosition != 0 ){
        //   let main = document.querySelector('main')
        //   let windowHeight = window.innerHeight
        //   let panel = document.querySelector('.recordPanel')
        //   let panelHeight = panel.offsetHeight
            // let recordHeight = document.getElementById( 'record'+id ).offsetHeight
        //   let panelTranslate = getComputedStyle(panel).transform
        //   let offsetToCenter = (windowHeight - panelHeight) / 2 + recordHeight // / 1.33
        //   recOffset = record.offsetTop - offsetToCenter

        //   if(debug) console.log(
        //     'windowHeight', windowHeight,
        //     'panelHeight', panelHeight,
        //     'panelTranslate', panelTranslate,
        //     'recordHeight', recordHeight,
        //     'offsetToCenter', offsetToCenter,
        //     'recordOffsetTop', record.offsetTop,
        //     'offset', recOffset,
        //     'record', record
        //   )
        // }else{
        //   recOffset = 0
        // }

        recOffset = record.offsetTop - record.offsetHeight/4

        window.scroll({
          top: recOffset,
          behavior: 'smooth'
        })
      })
    },

    setDateSelectState( state, newState ) {
      if(debug) console.log( 'setDateSelectState new state:', newState )
      state.dateSelectState = newState
    },

    recountBalance(state) {
      if(debug) console.log( 'recountBalance' )
      let summ = 0
      let summPlus = 0
      for( let i=0; i < state.records.length; i++ ){
        let rec = state.records[i]
        let price = Number( rec.price )
        let sign = rec.finalsign

        switch(sign) {
          case 'minus':
            summ = summ - price
          break

          case 'plus':
            summ = summ + price
            summPlus = summPlus + price
          break
        }
      }

      state.balance = summ
      state.balancePlus = summPlus
      state.balanceMinus = Math.abs( summ - summPlus )
    },

    updateLastId( state, payload ) {
      if(debug) console.log( 'updateLastId', payload )
      for( let i=0; i < Object.keys(payload).length; i++ ) {
        let key = Object.keys( payload )[i]
        state.lastId[key] = payload[key]
      }
    },


    ///////////////////////////////////////////////////// RECORDS ///////////////////////////////////////////////////////

    showBlankRecord(state) {
      let blankRecord = {}
      for( let i=0; i < Object.keys( state.blankRecord ).length; i++ ){
        let key = Object.keys( state.blankRecord )[i]
        blankRecord[key] = state.blankRecord[key]
      }
      Vue.set( state.records, 0, blankRecord )
      if(debug) console.log( 'showBlankRecord:', state.records[0] )
    },

    setRecordPanelState( state, newState ) {
      if(debug) console.log( 'setRecordPanelState:', newState )
      state.recordPanelStateLast = state.recordPanelState
      state.recordPanelState = newState
    },

    setCurrentRecordID( state, id ) {
      if(debug) console.log( 'setCurrentRecordID:', id )
      state.currentRecordID = id
      store.commit('updateCurrentRecordPosition')
    },

    updateCurrentRecordPosition( state, force = false) {
      let id = state.currentRecordID
      if(debug) console.log( 'updateCurrentRecordPosition fired, force?', force )
      if( id != state.lastId.records || force ) {
        for( let i=0; i < store.state.records.length; i++ ){
          let rec = store.state.records[i]
          if( rec.id == id ){
            if(debug) console.log( i, 'id:', id, 'rec.id', rec.id, id == rec.id )
            store.commit( 'setCurrentRecordPosition', i )
            break
          }
        }
      }else{
        store.commit( 'setCurrentRecordPosition', 0 )
      }
    },

    setCurrentRecordPosition( state, position ) {
      if(debug) console.log( 'setCurrentRecordPosition:', position )
      state.currentRecordPosition = position
    },

    addRecordProcedureStart( state) {
      if(debug) console.log( 'addRecordProcedureStart' )
      let newRecord = {
        id: state.lastId.records, // TODO
        hash: generateHash(),
        datetime: new Date().toISOString(),
        name: defaultRecordName,
        price: 0,
        sign: 'minus',
        finalsign: 'neutral',
        negative: false,
        deleted: false,
        selected: false,
        categories: {
          0: 0,
        },
      }

      store.commit( 'setCurrentRecordID', state.lastId.records )

      let blankRecord = state.records[0].blankRecord
      if( ! blankRecord ){
        state.records.unshift(newRecord);
      }else{
        Vue.delete( state.records[0], 'blankRecord' )
      }
      store.commit( 'changeRecord', newRecord )
    },

    cancelRecordAdd(state) {
      if(debug) console.log( 'cancelRecordAdd' )
      Vue.delete( state.records, state.currentRecordPosition )
    },

    saveCurrentRecordState(state) {
      let savedState = {}
      for( let i=0; i < Object.keys( state.records[ state.currentRecordPosition ] ).length; i++ ){
        let key = Object.keys( state.records[ state.currentRecordPosition ] )[i]
        savedState[key] = state.records[ state.currentRecordPosition ][key]
      }
      state.savedCurrentRecordState = savedState
      if(debug) console.log( 'saveCurrentRecordState:', state.savedCurrentRecordState )
    },

    cancelRecordEdit(state) {
      Vue.set( state.records, state.currentRecordPosition, state.savedCurrentRecordState )
      if(debug) console.log( 'cancelRecordEdit', state.records[ state.currentRecordPosition ] )
    },

    changeRecord( state, change ) {
      let n = state.currentRecordPosition
      if(debug) console.log( 'changeRecord fired:', change )
      for( let i=0; i < Object.keys(change).length; i++ ){
        let key = Object.keys(change)[i]
        let value = change[key]
        if( key != 'position' ){
          Vue.set( state.records[n], key, value )
          // state.records[n][key] = value
        }
      }
    },

    sortRecords(state) {
      if(debug) console.log('sortRecords fired')

      function compare(a,b) {
        if( a.datetime < b.datetime ){
          // if(debug) console.log( a.datetime, '<', b.datetime )
          return 1
        }else if( a.datetime > b.datetime )
          // if(debug) console.log( a.datetime, '>', b.datetime )
          return -1
        return 0
      }

      if( state.records.length > 1 ){
        state.records.sort(compare);
      }
    },

    selectRecord( state, id ) {
      if( !state.recordsSelected[id] ){
        Vue.set( state.recordsSelected, id, true )
      }else{
        Vue.delete( state.recordsSelected, id )
      }

      if( Object.keys( state.recordsSelected ).length < 1 ){
        state.appState = 'listPage'
      }else if( Object.keys( state.recordsSelected ).length == 1 ){
        state.appState = 'listOneRecordSelected'
      }else{
        state.appState = 'listMultipleRecordsSelected'
      }

      // if(debug) console.log('selectRecord fired, length:', Object.keys( state.recordsSelected ).length, state.appState )
    },

    unselectRecords(state) {
      if(debug) console.log( 'unselectRecords fired' )
      for( let i=0; i < Object.keys( state.recordsSelected ).length;  ){
        let id = Object.keys( state.recordsSelected )[i]
        Vue.delete( state.recordsSelected, id )
      }
    },

    deleteRecords(state) {
      if(debug) console.log( 'deleteRecords fired' )
      for( let i=0; i < Object.keys( state.recordsSelected ).length; i++ ){
        let id = Object.keys( state.recordsSelected )[i]
        for( let j=0; j < state.records.length; j++ ){
          let record = state.records[j]
          if( record.id == id ){
            state.records.splice( j, 1 )
            i--
            break
          }
        }
      }
      state.recordsSelected = []
      store.commit('recountBalance')
    },

    deleteRecord(state) {
      if(debug) console.log( 'deleteRecord fired', state.currentRecordPosition )
      state.records.splice( state.currentRecordPosition, 1 )
      store.commit('recountBalance')
    },


    ///////////////////////////////////////////////////// CATEGORIES /////////////////////////////////////////////////////////

    setCategoryPanelState( state, newState ) {
      if(debug) console.log( 'setCategoryEditPanelState:', newState )
      state.categoryPanelState = newState
    },

    setCategoryPanelLocalState( state, newState ) {
      if(debug) console.log( 'setCategoryEditPanelLocalState:', newState )
      state.categoryPanelLocalStatePrevious = state.categoryPanelLocalState+''
      state.categoryPanelLocalState = newState
    },

    setCurrentCategoryGroup( state, newGroupId ) {
      if(debug) console.log( 'setCurrentCategoryGroup:', newGroupId )
      state.currentCategoryGroup = newGroupId
    },


  }
});